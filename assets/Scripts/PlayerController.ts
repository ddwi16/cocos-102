import {
  _decorator,
  Component,
  EventMouse,
  Input,
  input,
  Vec3,
  Animation,
} from "cc";
const { ccclass, property } = _decorator;

export const BLOCK_SIZE = 40;

@ccclass("PlayerController")
export class PlayerController extends Component {
  @property(Animation)
  BodyAnim: Animation = null;

  private _startJump: boolean = false;
  private _jumpStep: number = 0;
  private _jumpTime: number = 0.1;

  private _currJumpTime: number = 0;
  private _currJumpSpeed: number = 0;

  private _currPos: Vec3 = new Vec3();
  private _deltaPos: Vec3 = new Vec3(0, 0, 0);
  private _targetPos: Vec3 = new Vec3();

  private _currMoveIndex: number = 0;

  start() {}

  reset() {
    this._currMoveIndex = 0;
  }

  setInputActive(active: boolean) {
    if (active) {
      input.on(Input.EventType.MOUSE_UP, this.onMouseUp, this);
    } else {
      input.off(Input.EventType.MOUSE_UP, this.onMouseUp, this);
    }
  }

  onMouseUp(event: EventMouse) {
    if (event.getButton() === EventMouse.BUTTON_LEFT) {
      this.jumpByStep(1);
    } else if (event.getButton() === EventMouse.BUTTON_RIGHT) {
      this.jumpByStep(2);
    }
  }

  jumpByStep(step: number) {
    if (this._startJump) {
      return;
    }
    this._startJump = true;
    this._jumpStep = step;
    this._currJumpTime = 0;

    const clipName = step == 1 ? "oneStep" : "twoStep";
    const state = this.BodyAnim.getState(clipName);
    this._jumpTime = state.duration;

    this._currJumpSpeed = (this._jumpStep * BLOCK_SIZE) / this._jumpTime;
    this.node.getPosition(this._currPos);
    Vec3.add(
      this._targetPos,
      this._currPos,
      new Vec3(this._jumpStep * BLOCK_SIZE, 0, 0)
    );

    if (this.BodyAnim) {
      if (step === 1) {
        this.BodyAnim.play("oneStep");
      } else if (step === 2) {
        this.BodyAnim.play("twoStep");
      }
    }

    this._currMoveIndex += step;
  }

  onOnceJumpEnd() {
    this.node.emit("JumpEnd", this._currMoveIndex);
  }

  update(deltaTime: number) {
    if (this._startJump) {
      this._currJumpTime += deltaTime;
      if (this._currJumpTime > this._jumpTime) {
        this.node.setPosition(this._targetPos);
        this._startJump = false;
        this.onOnceJumpEnd();
      } else {
        this.node.getPosition(this._currPos);
        this._deltaPos.x = this._currJumpSpeed * deltaTime;
        Vec3.add(this._currPos, this._currPos, this._deltaPos);
        this.node.setPosition(this._currPos);
      }
    }
  }
}
